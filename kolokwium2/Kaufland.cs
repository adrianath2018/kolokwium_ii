﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace kolokwium2
{
    class Kaufland : IDostawca
    {
        List<Produkt> produkty = new List<Produkt>()
        {
            new Produkt("Ciasto"),
            new Produkt("Szampon")
        };
        public Produkt Wyszukaj(string nazwa)
        {
            return produkty.FirstOrDefault(x => x.Nazwa == nazwa);
        }

        public void Zamow(Produkt produkt)
        {
            Console.WriteLine("Zamawiam " + produkt.Nazwa + " w " + this.ToString());
        }

        public override string ToString()
        {
            return "Kaufland";
        }
    }
}
