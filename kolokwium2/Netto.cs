﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kolokwium2
{
    class Netto : IDostawca
    {
        List<Produkt> produkty = new List<Produkt>()
        {
            new Produkt("Cola"),
            new Produkt("Jabłko")
        };
        public Produkt Wyszukaj(string nazwa)
        {
            return produkty.FirstOrDefault(x => x.Nazwa == nazwa);
        }

        public void Zamow(Produkt produkt)
        {
            Console.WriteLine("Zamawiam " + produkt.Nazwa + " w " + this.ToString());
        }

        public override string ToString()
        {
            return "Netto";
        }
    }
}
