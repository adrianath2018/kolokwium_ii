﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kolokwium2
{
    class Telefon
    {
        public string Name { get; set; }

        public Telefon(string name)
        {
            Name = name;
        }

        public void Dzwon(string number)
        {
            Console.WriteLine(Name + ". Dzwonie do " + number);
        }
    }
}
