﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kolokwium2
{
    interface IDostawca
    {
        public Produkt Wyszukaj(string nazwa);
        public void Zamow(Produkt produkt);
    }
}
