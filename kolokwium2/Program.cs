﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace kolokwium2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Zadanie 1\n");
            Random random = new Random();
            List<float> przelew = new List<float>();

            for (int i = 0; i < 100; i++)
                przelew.Add( (float)random.Next(-100000, 100000) / 100);

            Console.WriteLine("Max: " + przelew.Max());
            Console.WriteLine("Min: " + przelew.Min());
            Console.WriteLine("Sredia: " + przelew.Average());
            Console.WriteLine("Suma: " + przelew.Sum());
            Console.WriteLine("Dodatnich: " + przelew.Where(x => x > 0).Count());
            Console.WriteLine("Ujemnych: " + przelew.Where(x => x < 0).Count());

            Console.WriteLine("\n\nZadanie 2");

            Telefon telefon = new Telefon("Nokia3310");
            Smartfon smartfon = new Smartfon("XPhone");

            telefon.Dzwon("+48 888 888 888");
            smartfon.Dzwon("+48 777 777 777");
            smartfon.LadujStroneWWW("https://ath.edu.pl");

            Console.WriteLine("\n\nZadanie 3");

            List<IDostawca> dostawcy = new List<IDostawca>()
            {
                new Kaufland(),
                new Netto()
            };

            Console.WriteLine("Podaj nazwę produktu:");
            var produkt = Console.ReadLine();

            Produkt p = null;
            foreach (var d in dostawcy)
            {
                p = d.Wyszukaj(produkt);
                if (p != null)
                    d.Zamow(p);
            }
            if (p == null)
                Console.WriteLine("Brak tego produktu u każdego z dostawców");
            





        }

        

        
    }
}
